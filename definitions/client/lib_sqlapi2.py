
from requests import post


class SqlApiResponse:
    format: int = 0
    count: int = 0
    items: list[dict] | list[list[dict]] | None = []

    def __init__(self, items, count):
        self.items = items
        self.count = count

class SqlApiRequest:
    sql: str | list[str]
    params: dict | list[dict] | None = {}
    format: int = 0
    log_text: str = ""
    token: str = ""
    ignored_results: list[int] | None = None

    def __init__(self, sql, params={}, token=''):
        self.sql = sql
        self.params = params
        self.format = 0
        self.log_text = ""
        self.token = token


class SqlApiClient:
    """
    """

    _CONNECTION_TIMEOUT = 10
    _RECEIVE_TIMEOUT = 30

    def __init__(self, url, use_auth=False):
        self._url = url
        self._use_auth = use_auth

    def execute(self, sql: str, params: dict = {}, delCRLF = True):
        try:
            sql = _removeCRLF(sql) if delCRLF else sql
            if (r := post(self.url, timeout=(self._CONNECTION_TIMEOUT, self._RECEIVE_TIMEOUT),
                          json=SqlApiRequest(sql=sql, params=params, token=self.auth_token).__dict__)).status_code == 200:
                data = r.json()
                return (SqlApiResponse(data["items"], data["count"]), "")
            else:
                return (None, r.text)
        except Exception as e:
            return (None, str(e))

    @property
    def url(self):
        return self._url

    @property
    def use_auth(self):
        return self._use_auth

    @property
    def auth_token(self):
        return ''



def _removeCRLF(txt: str):
    return txt.replace('\r', '').replace('\n', ' ') if txt else txt


'''
Adres serwera dla endpointow z danymi przykladowymi:
 http://srv1.aws.rptalent.eu:8111

Ponieważ serwis rest jest napisany w FastApi, a ono jest w pewnym stopniu samodokumentujące, to
aby zobaczyć sobie dostępne endpointy i w jakim formacie wysyłać do nich requesty, wystarczy:
 http://srv1.aws.rptalent.eu:8111/docs

W tej chwili endpoint dla POSTa, serwuje dane z dwóch baz (to są MySQL fizycznie) o nazwach test1 i test2.
Test2, to przykładowa baza żywcem ściągnieta ze strony MySQLa - udostępniają taką. 
Test1, to baza tylko z 1 tabelą od nas z AMW.
Żeby sobie zobaczyć jak to działa, przykładowo należy:
Dla http://srv1.aws.rptalent.eu:8111/docs w endpoincie POST (po wybraniu Try it out):
Jako db_name podać test1. Jako Request Body przekleić jeden z poniższych przykładów.

{
"sql": "SELECT * FROM vevents ORDER BY event_logid DESC LIMIT 20",
"params": {},
"format": 0,
"log_text": "",
"token": "",
"ignored_results": []
}
2.

{
  "sql": "SELECT * FROM vevents WHERE event_reasonid = :reason ORDER BY event_logid LIMIT 20",
  "params": {"reason": 2},
  "format": 0,
  "log_text": "",
  "token": "",
  "ignored_results": []
}

3.

{
  "sql": "SELECT DATE(event_time) data, count(*) poprawnych_logowan FROM vevents WHERE event_reasonid = :reason GROUP BY DATE(event_time) LIMIT 20",
  "params": {"reason": 1},
  "format": 0,
  "log_text": "",
  "token": "",
  "ignored_results": []
}

4.

{
  "sql": "SELECT * FROM vevents WHERE event_reasonid = :reason AND ipaddress LIKE :ip ORDER BY event_logid LIMIT 20",
  "params": {"reason": 1, "ip": "10.%"},
  "format": 0,
  "log_text": "",
  "token": "",
  "ignored_results": []
}

Po wykonaniu zapytania z poziomu interfejsu www (lub innego interfejsu - np zwyklym curl) widac postac odpowiedzi:
Jest ona co do strukrury zawsze taka sama, o ile kod http odpowiedzi wynosi 200.

Dla przykladu 3, odpwiedz ma postac:

{
  "format": 0,
  "count": 20,
  "items": [
    {
      "data": "2023-07-22",
      "poprawnych_logowan": 7
    },
    {
      "data": "2023-07-24",
      "poprawnych_logowan": 394
    },
    {
      "data": "2023-07-25",
      "poprawnych_logowan": 315
    },
    ...
   ]
}

Pole items zawiera listę słowników, których pola wynikją z pól okreslonych w zapytaniu - może być to lista pusta, jeżeli zapytanie nic nie zwraca.
Pole count - dodatkowo zawiera liczbę elementów w items, lub np. liczbę zmodyfikowanyc/dodanych wierszy - to zależy od rodzaju zapytania.
Jedyne na co teraz należy zwrócić uwagę, to to aby nie pobierać z tego endpointa zbyt dużo danych - max koło 50 rekordów. Chodzi tylko o
wydajność tej maszyny wirtulnej na której to pracuje - jest bardzo niska. Dlatego w zapytaniach trzeba wymuszać na razie LIMIT.
'''


# - kategorie zapytan jako checkboxy w widoku SQLQuery
# SQLParam dowiązuje do SQLQuery (name, description - widoczne )