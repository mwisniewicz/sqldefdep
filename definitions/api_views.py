from rest_framework import viewsets
from rest_framework.views import APIView
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from definitions.seralizers import UserSerializer, SQLQueryRequestSeralizer
from definitions.models import SQLQueryRequest

from rest_framework.permissions import BasePermission, SAFE_METHODS
from rest_framework.response import Response
from rest_framework import status



class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class SQLQueryRequesViewSet(viewsets.ModelViewSet):
    queryset = SQLQueryRequest.objects.all()
    serializer_class = SQLQueryRequestSeralizer


class ModifySessionView(APIView):

    def patch(self, request):
        session_key = request.data.get('session_key')
        if session_key:
            try:
                session = Session.objects.get(session_key=session_key)
            except Session.DoesNotExist:
                return Response(
                    {'success': False,
                     'message': 'Session not found'},
                     status=status.HTTP_404_NOT_FOUND
                )
            else:
                session_data = session.get_decoded()
                print(">>>>>>>>>>>>>>>>>>")
                print(session_data)
                print("#################")
                session_data['test_name'] = 'TEST'
                session.modified = True
                session.save()
                print("<<<<<<<<<<<<<<<<<<<<<<<<<")

                return Response(
                    {'success': True,
                     'message': 'Session modified',
                     'session_data': session_data},
                    status=status.HTTP_200_OK
                )
        else:
            return Response(
                {'success': False,
                 'message': 'Session key not provided'},
                status=status.HTTP_400_BAD_REQUEST
            )
