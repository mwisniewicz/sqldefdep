from django.urls import path
from definitions.consumers import TaskConsumer


websocket_urlpatterns = [
    path("ws/tasks/mateusz/", TaskConsumer.as_asgi()),
]