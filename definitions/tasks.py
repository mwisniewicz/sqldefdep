from time import sleep

from django.apps import apps
from celery import shared_task


@shared_task
def register_sqlqueryrequest(sqlqueryrequest_id):
    print(">> TASK REGISTERED")
    sleep(3)
    obj = apps.get_model('definitions.SQLQueryRequest').objects.get(id=sqlqueryrequest_id)
    obj.process()
    print(">> TASK DONE >>")
