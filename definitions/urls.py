"""
URL configuration for sqldefdep project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from definitions.views import (
    SQLCategoryList,
    SQLQueryCategoryList,
    SQLQueryTemplate,
    SQLQueryResult,
    SQLQueryView,
    ReportList,
    SQLQueryRequestDetail
)

urlpatterns = [
    path('', ReportList.as_view(), name='reports'),
    path('categories/', SQLCategoryList.as_view(), name='categories'),
    path('zapytania/<int:category_id>/', SQLQueryCategoryList.as_view(), name='sql_query_categories'),
    path('raport/<int:sql_query_id>/', SQLQueryTemplate.as_view(), name='sql_query'),
    path('report/result/', SQLQueryResult.as_view(), name='report_result'),
    path('query/', SQLQueryView.as_view(), name='query-create'),
    path('result/<int:pk>/', SQLQueryRequestDetail.as_view(), name='result')
]
