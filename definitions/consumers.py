import json
import time
from channels.generic.websocket import WebsocketConsumer

from asgiref.sync import async_to_sync


class TaskConsumer(WebsocketConsumer):

    def connect(self):
        self.group = 'mateusz'
        async_to_sync(self.channel_layer.group_add)(self.group, self.channel_name)
        print("Connected to Websocket")
        print("Channel group: ", self.group)
        print("Channel name: ", self.channel_name)
        self.accept()
        #self.send(text_data=json.dumps({
        #    'message': 'TO jest odppowiedz od serwera'
        #}))

    def disconnect(self, close_code):
        print("Websocket Disconnected")
        async_to_sync(self.channel_layer.group_discard)(self.group, self.channel_name)

    def receive(self, text_data):
        print("Reciever dostał sygnał...")
        data = json.loads(text_data)
        print(data.get("message"))
        time.sleep(5)
        self.send(text_data=json.dumps({
            'message': 'TO jest odppowiedz od serwera'
        }))

    def taks_completed(self, data, **kwargs):
        print("Handler Start")
        self.send(text_data=json.dumps({
            'message': 'task_done',
            'data': json.dumps(data)
        }))