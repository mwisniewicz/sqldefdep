from django import forms
from django.forms import modelformset_factory
from definitions.models import SQLQuery, SQLParam

from django.core.exceptions import ValidationError

class SQLQueryParamsForm(forms.Form):

    def __init__(self, params_dict, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for param_name, param_data in params_dict.items():
            param_type = param_data['param_type'].title()
            self.fields[param_name] = getattr(forms, f'{param_type}Field')(
                initial=param_data['value'],
                label=param_data['label'],
                required=param_data['required']
                )
        
            


class SQLQueryForm(forms.ModelForm):

    class Meta:
        model = SQLQuery
        fields = ['raw_sql', 'api', 'name']


    def clean_name(self):
        print("#####################")
        data = self.cleaned_data['name']
        if len(data) < 4:
            raise ValidationError("Za krótka nazwa!")
        return data



SQLParamFormSet = modelformset_factory(
    SQLParam, fields=('name', 'description', 'param_type', 'required', 'default_value'), extra=1, max_num=5
)
