from django.contrib import admin
from django.contrib.auth.models import User
from unfold.admin import ModelAdmin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import (
    SQLCategory,
    APIEndpoint,
    SQLQuery,
    SQLParam,
    SQLQueryParam,
    SQLQueryCategory,
    SQLCategoryUser
)


class APIEndpointAdmin(ModelAdmin):
    list_display = ("name", "url")


class SQLParamInline(admin.TabularInline):
    model = SQLQueryParam
    extra = 1


class SQLQueryAdmin(ModelAdmin):
    list_display = ("name", "api", "short_sql_raw")
    inlines = [SQLParamInline]


class SQLParamAdmin(ModelAdmin):
    list_display = ("name", "description", "param_type", "required", "default_value")



admin.site.register(SQLCategory)
admin.site.register(APIEndpoint, APIEndpointAdmin)
admin.site.register(SQLParam, SQLParamAdmin)
admin.site.register(SQLQuery, SQLQueryAdmin)
admin.site.register(SQLQueryCategory)

admin.site.register(SQLCategoryUser)


admin.site.unregister(User)

@admin.register(User)
class UserAdmin(BaseUserAdmin, ModelAdmin):
    pass