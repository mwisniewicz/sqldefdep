from typing import Any
from django.apps import apps
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
from django.db.models.query import QuerySet
from django.views.generic import TemplateView, ListView, FormView, DetailView
from django.urls import reverse

from django.shortcuts import redirect

from definitions.client.lib_sqlapi2 import SqlApiClient
from definitions.forms import SQLQueryParamsForm, SQLQueryForm, SQLParamFormSet
from definitions.models import SQLCategoryUser, SQLQueryCategory, SQLParam, SQLQueryRequest

import pprint

import datetime

class HomeView(TemplateView):
    template_name = 'home.html'


    def get_context_data(self, **kwargs):
        data = self.get_data()
        context = super().get_context_data(**kwargs)
        context["columns"] = data[0].keys()
        context["data"] = data
        return context
    
    def get_data(self):
        query = apps.get_model('definitions.SQLQuery').objects.get()
        URL = query.api.url
        PARAMS = query.get_params()
        SQL_RAW = query.raw_sql
        response, _ = SqlApiClient(URL).execute(SQL_RAW, PARAMS)
        return response.items


class SQLCategoryList(LoginRequiredMixin, ListView):
    model = SQLCategoryUser
    template_name = 'categories/category_list.html'
    context_object_name = 'sql_categories'

    def get_queryset(self):
        return SQLCategoryUser.objects.filter(user=self.request.user)



class SQLQueryCategoryList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'categories/category_query_list.html'
    model = SQLQueryCategory

    def get_queryset(self):
        return SQLQueryCategory.objects.filter(sql_category__id=self.kwargs['category_id'])
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category_name'] = self.get_category().name
        return context
    
    def get_category(self):
        return apps.get_model('definitions.SQLCategory').objects.get(id=self.kwargs['category_id'])
    
    def has_permission(self):
        try:
            apps.get_model('definitions.SQLCategoryUser').objects.get(
                user=self.request.user,
                sql_category=self.get_category()
            )
        except ObjectDoesNotExist:
            return False
        return True


class SQLQueryTemplate(LoginRequiredMixin, FormView):
    template_name = 'queries/sql_query.html'
    form_class = SQLQueryParamsForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['report_name'] = self.get_sql_query().name
        context['category_obj'] = self.get_sql_query().sql_categories.last
        return context

    def get_sql_query(self):
        return apps.get_model('definitions.SQLQuery').objects.get(id=self.kwargs['sql_query_id'])
    
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['params_dict'] = self.get_sql_query().get_params()
        return kwargs
    
    def get_sql_params(self, form_data):
        param_dict = {}
        for name, value in form_data.items():
            if isinstance(value, datetime.date):
                value = value.strftime("%Y-%m-%d")
            param_dict[name] = value
        return param_dict
    
    def form_valid(self, form):
        print("FORM VALIDATED")
        form_data = form.cleaned_data
        apps.get_model('definitions.SQLQueryRequest').objects.create(
            user=self.request.user,
            sql_query=self.get_sql_query(),
            sql_params=self.get_sql_params(form_data)
        )
        return super().form_valid(form)
    
    def get_success_url(self):
        return reverse('reports')



class SQLQueryRequestDetail(DetailView):
    model = SQLQueryRequest
    template_name = 'queries/result.html'

    def get_page_obj(self):
        paginator = Paginator(self.object.output_data, 5)
        page_number = self.request.GET.get('page')
        return paginator.get_page(page_number)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_obj'] = self.get_page_obj()
        return context


class ReportList(ListView):
    template_name = 'queries/reports.html'
    paginate_by = 10

    def get_queryset(self):
        return apps.get_model('definitions.SQLQueryRequest').objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        pprint.pprint(self.request.session.__dict__)
        return super().get_context_data(**kwargs)

class SQLQueryResult(LoginRequiredMixin, ListView):
    template_name = 'queries/result.html'
    #paginate_by = 5
    
    def get_queryset(self):
        data = self.get_data()
        return data

    def get_data(self):
        query = apps.get_model('definitions.SQLQuery').objects.first()
        return []
        #URL = query.api.url
        #PARAMS = query.get_params()['']
        #SQL_RAW = query.raw_sql
        #response, _ = SqlApiClient(URL).execute(SQL_RAW, PARAMS)
        #return response.items
    

class SQLQueryView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'admin/query.html'

    def has_permission(self):
        if self.request.user.is_superuser:
            return True
        return False
    
    def get(self, *args, **kwargs):
        main_form = SQLQueryForm()
        params_formset = SQLParamFormSet(queryset=SQLParam.objects.none())
        return self.render_to_response(
            {
                'main_form': main_form,
                'params_formset': params_formset
            }
        )
    

    def post(self, *args, **kwargs):
        print("TEST POST")
        pprint.pprint(self.request.POST)
        params_formset = SQLParamFormSet(queryset=SQLParam.objects.none())
        main_form = SQLQueryForm(self.request.POST)
        if main_form.is_valid():
            params_formset = SQLParamFormSet(data=self.request.POST)
            if params_formset.is_valid():

                name = self.request.POST.get('name')
                raw_sql = self.request.POST.get('raw_sql')
                api_id = self.request.POST.get('api')
                obj = self.create_sql_raw_obj(name, raw_sql, api_id)
                self.create_full_sql_raw(obj)
                return redirect(reverse('home'))
            else:
                print(">>>>>>>>>>")
                print(params_formset.__dict__)
                return self.render_to_response(
                {
                'main_form': main_form,
                'params_formset': params_formset
                }
            )
        else:
            print(">>>>>>>>>>>>>>>>")
            print(main_form.errors)
            return self.render_to_response(
                {
                'main_form': main_form,
                'params_formset': params_formset
                }
            )

    def create_sql_raw_obj(self, name, raw_sql, api_id):
        api_obj = apps.get_model('definitions.APIEndpoint').objects.get(id=api_id)
        return apps.get_model('definitions.SQLQuery').objects.create(
            name=name,
            raw_sql=raw_sql,
            api=api_obj
        )
    

    def create_full_sql_raw(self, sql_quer):
        data=self.request.POST
        pass