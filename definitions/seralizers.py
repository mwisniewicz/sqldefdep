from rest_framework import serializers
from django.contrib.auth.models import User
from definitions.models import SQLQueryRequest

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff', 'is_active']


class SQLQueryRequestSeralizer(serializers.ModelSerializer):
    class Meta:
        model = SQLQueryRequest
        fields = ['get_username', 'publication_datetime', 'start_datetime', 'end_datetime',
                  'status', 'log_error', 'sql_query', 'sql_params', 'output_data']
