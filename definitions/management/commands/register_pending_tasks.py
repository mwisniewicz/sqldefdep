from django.core.management.base import BaseCommand

from definitions.models import SQLQueryRequest
from definitions.tasks import register_sqlqueryrequest

class Command(BaseCommand):

    #def add_arguments(self, parser):
    #    parser.add_argument('test_param')

    def handle(self, *args, **options):
        #print(options['test_param'])
        pending_requests = SQLQueryRequest.objects.filter(status='pending')
        if not pending_requests.exists():
            self.stdout.write(
                self.style.ERROR("BRAK ZADAN")
            )
        for obj in pending_requests:
            print("TASK SEND TO CELERY >> ", obj)
            register_sqlqueryrequest.delay(obj.id)
        self.stdout.write(
                self.style.SUCCESS("PROCES ZAKONCZONO")
            )

# python -m celery -A  worker -l info