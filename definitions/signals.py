from definitions.models import SQLQueryRequest
from django.db.models.signals import post_save
from django.dispatch import receiver

from definitions.tasks import register_sqlqueryrequest

@receiver(post_save, sender=SQLQueryRequest)
def handle_sql_query_request(sender, instance, created, **kwargs):
    if created:
        register_sqlqueryrequest.delay(instance.id)
