from django.db import models
from django.apps import apps
import datetime 

import time

from django.core.exceptions import ValidationError

from channels.layers import get_channel_layer

from asgiref.sync import async_to_sync

from definitions.client.lib_sqlapi2 import SqlApiClient

PARAM_TYPES = (
    ('char', 'Tekst'),
    ('integer', 'Liczba całkowita'),
    ('date', 'Data'),
    ('float', 'Liczba rzeczywista'),
    ('boolean', 'Typ logiczny')
)


SQL_REQUEST_STATUSES = (
    ('pending', 'Oczekuje'),
    ('running', 'W trakcie'),
    ('done', 'Zakończono'),
    ('error', 'Błąd')
)

class APIEndpoint(models.Model):
    name = models.CharField(max_length=50)
    url = models.URLField()

    class Meta:
        verbose_name = 'Endpoint'
        verbose_name_plural = 'Endpointy'

    def __str__(self):
        return self.name


class SQLQuery(models.Model):
    raw_sql = models.TextField()
    api = models.ForeignKey(APIEndpoint, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'Zapytanie SQL'
        verbose_name_plural = 'Zapytania SQL'

    def __str__(self):
        return self.name
    
    def clean(self):
        if len(self.name) < 5:
            raise ValidationError("Za krótka nazwa zapytania")
    

    def short_sql_raw(self):
        return self.raw_sql[:20] + "..."

    def get_params(self):
        param_dict = {}
        params = apps.get_model('definitions.SQLQueryParam').objects.filter(sql_query=self)
        for param in params:
            param_dict[param.sql_param.name] = {
                'value': param.sql_param.get_value(),
                'param_type': param.sql_param.param_type,
                'label': param.sql_param.description,
                'required': param.sql_param.required
            }
        return param_dict


class SQLParam(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    param_type = models.CharField(max_length=7, choices=PARAM_TYPES)
    required = models.BooleanField()
    default_value = models.CharField(max_length=50)
    
    class Meta:
        verbose_name = "Parametr"
        verbose_name_plural = "Parametry"

    def clean(self):
        #if len(self.name) < 5:
        #    raise ValidationError("Pole nazwa ma za krótka nazwa parametru")
        pass

    def __str__(self):
        return f'{self.name} - {self.description}'
    
    def get_value(self):
        if self.param_type == 'i':
            return int(self.default_value)
        elif self.param_type == 'r':
            return float(self.default_value)
        #elif self.param_type == 'd':
        #    return datetime.datetime.strptime(self.default_value, '%Y-%m-%d').date()
        return self.default_value


class SQLQueryParam(models.Model):
    sql_query = models.ForeignKey(SQLQuery, on_delete=models.CASCADE)
    sql_param = models.ForeignKey(SQLParam, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Parametr Zapytania"
        verbose_name_plural = "Parametry zapytania"



class SQLCategory(models.Model):
    name = models.CharField(max_length=50, verbose_name='Nazwa kategorii')

    class Meta:
        verbose_name = 'Kategoria zapytania SQL'
        verbose_name_plural = 'Kategorie zapytań SQL'

    def __str__(self):
        return self.name


class SQLQueryCategory(models.Model):
    sql_query = models.ForeignKey(SQLQuery, on_delete=models.CASCADE, related_name='sql_categories')
    sql_category = models.ForeignKey(SQLCategory, on_delete=models.CASCADE)


class SQLCategoryUser(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    sql_category = models.ForeignKey(SQLCategory, on_delete=models.CASCADE)


class SQLQueryRequest(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    publication_datetime = models.DateTimeField(auto_now_add=True, null=True)
    start_datetime = models.DateTimeField(null=True)
    end_datetime = models.DateTimeField(null=True)
    status = models.CharField(choices=SQL_REQUEST_STATUSES, default='pending', max_length=7)
    log_error = models.TextField(default='')
    sql_query = models.ForeignKey('definitions.SQLQuery', on_delete=models.CASCADE)
    sql_params = models.JSONField()
    output_data = models.JSONField(null=True)

    class Meta:
        ordering = ["-id"]

    def get_username(self):
        return self.user.username
    
    def get_color_for_status(self):
        colors = {
            'pending':'blue',
            'running': 'yellow',
            'done': 'green',
            'error': 'red'
        }
        return colors[self.status]

    def process(self):
        channel_layer = get_channel_layer()
        print("START")
        self.start_datetime = datetime.datetime.now()
        self.status = 'running'
        async_to_sync(channel_layer.group_send)("mateusz", {
            "type": "taks_completed", "data": {"id": self.id, "status": self.status, "status_name": self.get_status_display()}
        })
        self.save()
        URL = self.sql_query.api.url
        PARAMS = self.sql_params
        SQL_RAW = self.sql_query.raw_sql
        response, error_text = SqlApiClient(URL).execute(SQL_RAW, PARAMS)
        if error_text:
            self.status = 'error'
        else:
            self.output_data = response.items
            self.status = 'done'
        self.end_datetime = datetime.datetime.now()
        self.save()
        print("END")
    
        async_to_sync(channel_layer.group_send)("mateusz", {
            "type": "taks_completed", "data": {"id": self.id, "status": self.status, "status_name": self.get_status_display()}
        })



'''
	
r.prochnicki
08:37







Adres serwera dla endpointow z danymi przykladowymi:
 http://srv1.aws.rptalent.eu:8111

Ponieważ serwis rest jest napisany w FastApi, a ono jest w pewnym stopniu samodokumentujące, to
aby zobaczyć sobie dostępne endpointy i w jakim formacie wysyłać do nich requesty, wystarczy:
 http://srv1.aws.rptalent.eu:8111/docs

W tej chwili endpoint dla POSTa, serwuje dane z dwóch baz (to są MySQL fizycznie) o nazwach test1 i test2.
Test2, to przykładowa baza żywcem ściągnieta ze strony MySQLa - udostępniają taką. 
Test1, to baza tylko z 1 tabelą od nas z AMW.
Żeby sobie zobaczyć jak to działa, przykładowo należy:
Dla http://srv1.aws.rptalent.eu:8111/docs w endpoincie POST (po wybraniu Try it out):
Jako db_name podać test1. Jako Request Body przekleić jeden z poniższych przykładów.

{
"sql": "SELECT * FROM vevents ORDER BY event_logid DESC LIMIT 20",
"params": {},
"format": 0,
"log_text": "",
"token": "",
"ignored_results": []
}
2.

{
  "sql": "SELECT * FROM vevents WHERE event_reasonid = :reason ORDER BY event_logid LIMIT 20",
  "params": {"reason": 2},
  "format": 0,
  "log_text": "",
  "token": "",
  "ignored_results": []
}

3.

{
  "sql": "SELECT DATE(event_time) data, count(*) poprawnych_logowan FROM vevents WHERE event_reasonid = :reason GROUP BY DATE(event_time) LIMIT 20",
  "params": {"reason": 1},
  "format": 0,
  "log_text": "",
  "token": "",
  "ignored_results": []
}

4.

{
  "sql": "SELECT * FROM vevents WHERE event_reasonid = :reason AND ipaddress LIKE :ip ORDER BY event_logid LIMIT 20",
  "params": {"reason": 1, "ip": "10.%"},
  "format": 0,
  "log_text": "",
  "token": "",
  "ignored_results": []
}'''