"""
URL configuration for sqldefdep project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from definitions.api_views import UserViewSet, SQLQueryRequesViewSet, ModifySessionView

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'reports', SQLQueryRequesViewSet)
#router.register(r'modify-session', ModifySessionView, basename='session-modify')


urlpatterns = [
    path('admin/', admin.site.urls),
    path("accounts/", include("django.contrib.auth.urls")),
    path("", include("definitions.urls")),
    path('api-auth/', include('rest_framework.urls')),
    path('modify-session', ModifySessionView.as_view()),
    path('', include(router.urls)),

]
