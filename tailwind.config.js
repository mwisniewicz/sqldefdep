/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./definitions/templates/**/*.html'],
  theme: {
    extend: {},
  },
  plugins: [require('flowbite/plugin')],
}

