
from lib_sqlapi2 import SqlApiClient, SqlApiResponse


URL = 'http://srv1.aws.rptalent.eu:8111/api/'
TEST1 = f'{URL}test1'

SQL = 'SELECT * FROM vevents LIMIT :limit'
PARAMS = {'limit': 20}

# 1 OK
data, err_text = SqlApiClient(TEST1).execute(SQL, PARAMS)
if data:
    [print(item) for item in data.items]
else:
    print(err_text)

# 1 ERROR
SQL = 'SELECT * FROM veventsxx LIMIT 10'
data, err_text = SqlApiClient(TEST1).execute(SQL)
if data:
    [print(item) for item in data.items]
else:
    print(err_text)


